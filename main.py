# Entry point for the program see README.md for details
from player_reader import NflPlayerReader
from lineup_generator import LineupGenerator
import sys

if __name__ == "__main__":
  max_salary = 50000
  reader = NflPlayerReader(sys.argv[1], sys.argv[2], float(sys.argv[3]))
  players = reader.load_players()
  optimalLineup = LineupGenerator(players, max_salary).find();
  print(optimalLineup)
