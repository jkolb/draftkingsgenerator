# This class represents an nfl player
# A player has a name, position, salary, and point value
class NflPlayer:
  def __init__(self, name, team, position, points, salary):
    self.name = name
    self.team = team
    self.position = position
    self.salary = salary
    self.points = int(points)

  def __str__(self):
    return  '\nName: ' + self.name + '\nTeam: ' + self.team + '\nPosition: ' + self.position + '\nPoints: ' + str(self.points) + '\nSalary: ' + str(self.salary)

  def __repr__(self):
    return self.__str__()
