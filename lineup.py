# This class represents a DraftKings line up. It contains a list of players
# and the aggrigate of their points and salary. This class does no internal
# validation and it is the responsibiliy of the consumer to makes any checks on
# a lineup's validity
class NflLineup:
  def __init__(self, players):
    self.players = list(players)
    self.points = 0
    self.salary = 0

    for player in self.players:
      self.points += player.points
      self.salary += player.salary

  def __str__(self):
    return 'Salary: ' + str(self.salary) + ' Points: ' + str(self.points) +'\n Players: ' + str(self.players)

  def __repr__(self):
    return self.__str__()
