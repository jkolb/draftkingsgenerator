# This class is responsible for reading the provided csv files and
# creating the player objects from the composition of the csv row data.
import csv
from player import NflPlayer
class NflPlayerReader:
  def __init__(self, player_file, projections_file, historic_weight):
    self.player_file = player_file
    self.projections_file = projections_file
    self.historic_weight = historic_weight

  def load_players(self):
    players = [];
    projections_dict = dict();
    projection_reader = csv.DictReader(open(self.projections_file))
    for row in projection_reader:
      projections_dict[row['Player Name']] = float(row['fpts'])


    reader = csv.DictReader(open(self.player_file))
    for row in reader:
      #Remove hyphens in name to prevent issues with how the PuLP lib works.
      name = row['Name'].strip()
      position = row['Position']
      projected_points = 0
      historic_avg_points = float(row['AvgPointsPerGame'])
      team = row['teamAbbrev']

      try:
        projected_points = projections_dict[name]
      except:
        projected_points = historic_avg_points
        try:
          if position != "DST":
            name = name.replace("Jr.", "").replace("Sr.", "")
            projected_points = projections_dict[name]
        except:
          continue

      if projected_points > 0:
        projected_weight = 1 - self.historic_weight
        points = self.historic_weight * historic_avg_points + projected_weight * projected_points
      else:
        points = 0
      players.append(NflPlayer(name.replace("-", " "), team, position, points, int(row['Salary'])))

    return players
