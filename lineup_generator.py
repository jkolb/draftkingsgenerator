# The lineup generator takes a list of players and comes up with a valid Draft kings line up.
# A valid lineup is defined as:
# - 9 players
# - 1 QB
# - 1 DST
# - >= 1 TE
# - >= 2 RB
# - >= 3 WR
# Please note that the total size and minimum bounds on TE,WR, and RB leave room
# for 1 flex player.
#
from pulp import *
from lineup import NflLineup

class LineupGenerator:
  def __init__(self, players, max_salary):
    self.players = players
    self.max_salary = max_salary
    #Name to player
    self.player_dict = {}
    #Position to player name
    self.player_position = {
      'QB': [],
      'RB': [],
      'WR': [],
      'TE': [],
      'DST': []
    }

    self.teams = set()

    for player in self.players:
      self.teams.add(player.team)
      self.player_dict[player.name] = player
      if player.position == 'QB':
        self.player_position['QB'].append(player)
      if player.position == 'WR':
        self.player_position['WR'].append(player)
      if player.position == 'RB':
        self.player_position['RB'].append(player)
      if player.position == 'TE':
        self.player_position['TE'].append(player)
      if player.position == 'DST':
        self.player_position['DST'].append(player)


  # This method will cause the optimal lineup to be constructed from the players
  # availible in the instance of the LineupGenerator
  def find(self):
    player_vars = LpVariable.dicts("Players", self.player_dict.keys(), 0, 1, cat='Integer')

    prob = LpProblem("The Lineup Problem", LpMaximize)
    # Linup points
    prob += lpSum(self.player_dict[i].points * player_vars[i] for i in self.player_dict.keys()), "Total Linup Points"

    #DK Constraints
    prob += lpSum(self.player_dict[i].salary * player_vars[i] for i in self.player_dict.keys()) <= self.max_salary, "Salary Requirement"
    prob += lpSum(player_vars[i] for i in self.player_dict.keys()) == 9, "LineupSize"
    prob += lpSum(player_vars[i.name] for i in self.player_position['QB']) == 1, "qbLimit"
    prob += lpSum(player_vars[i.name] for i in self.player_position['DST']) == 1, "dstMin"
    prob += lpSum(player_vars[i.name] for i in self.player_position['WR']) >= 3, "wrMin"
    prob += lpSum(player_vars[i.name] for i in self.player_position['RB']) >= 2, "rbMin"
    prob += lpSum(player_vars[i.name] for i in self.player_position['TE']) >= 1, "teMin"

    #Limit Players on the same team
    #WR limit for each team
    for team in self.teams:
      wrConstraintName = "wrTeam" + team
      rbConstraintName = "rbTeam" + team

      teamWrs = []
      teamRbs = []
      for i in self.player_position['WR']:
        if i.team == team:
          teamWrs.append(player_vars[i.name])

      for i in self.player_position['RB']:
        if i.team == team:
          teamRbs.append(player_vars[i.name])

      prob += lpSum(teamWrs) <= 1, wrConstraintName
      prob += lpSum(teamRbs) <= 1, rbConstraintName

    #This will write out a file that contains the break down of the equations
    prob.writeLP("LineupModel.lp")
    prob.solve()

    print("Status:", LpStatus[prob.status])

    lineup_players = []
    for var in prob.variables():
      if var.varValue == 1:
        name = var.name.replace("Players_", "").replace("_", " ", 100)
        lineup_players.append(self.player_dict[name])

    return NflLineup(lineup_players)
